package ru.sber.spring.SpringHomeWork.dbexample.model;

import lombok.*;

import java.util.Date;
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    private Integer booksId;
    private String booksTitle;
    private String booksAutor_book;
    private Date dateAdded;
}
