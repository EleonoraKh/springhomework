package ru.sber.spring.SpringHomeWork.dbexample.model;

import lombok.*;

import java.time.LocalDate;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

public class User {

    private Integer userId;
    private String userLastName;
    private String userFirstName;
    private LocalDate userDateOfBirth;
    private String userTelephone;
    private String userMail;
    private String userListBook;
}
