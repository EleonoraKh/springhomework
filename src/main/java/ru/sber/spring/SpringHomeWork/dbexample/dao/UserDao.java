package ru.sber.spring.SpringHomeWork.dbexample.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.sber.spring.SpringHomeWork.dbexample.model.User;


import java.sql.*;

@Component
@Scope("prototype")
public class UserDao {
    private final BookDao bookDao;
    private final Connection connection;

    public UserDao(Connection connection, BookDao bookDao){
        this.connection = connection;
        this.bookDao = bookDao;
    }

    public void addUser(User user) throws SQLException {
        PreparedStatement addQuery = connection.prepareStatement("insert into users_new" +
                "(last_name, first_name, date_of_birth, telephone, mail, list_books)" +
                "values (?, ?, ?, ?, ?, ?)");
        addQuery.setString(1, user.getUserLastName());
        addQuery.setString(2, user.getUserFirstName());
        addQuery.setDate(3, Date.valueOf(user.getUserDateOfBirth()));
        addQuery.setString(4, user.getUserTelephone());
        addQuery.setString(5, user.getUserMail());
        addQuery.setString(6, user.getUserListBook());
        addQuery.executeUpdate();
    }

    public void getUserBooksInfo(String userTelephone) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select list_books from users_new" +
                " where telephone = ?");
        selectQuery.setString(1, userTelephone);
        ResultSet result = selectQuery.executeQuery();
        while (result.next()){
            String s = result.getString(1);
            String[] titles = s.split(", ");
            for (String e : titles){
                System.out.println(bookDao.findBookByTitle(e));
            }
        }
    }

    public User findUserById(Integer userId) throws SQLException {
        String sql = "select * from user where id = ?";
        PreparedStatement selectQuery = connection.prepareStatement(sql);
        selectQuery.setInt(1, userId);
        ResultSet result = selectQuery.executeQuery();
        User user = new User();
        while (result.next()) {
            user.setUserId(result.getInt("id"));

            System.out.println(user);
        }
        return user;
    }
}
