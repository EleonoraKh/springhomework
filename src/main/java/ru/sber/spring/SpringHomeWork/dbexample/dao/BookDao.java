package ru.sber.spring.SpringHomeWork.dbexample.dao;

import org.springframework.stereotype.Component;
import ru.sber.spring.SpringHomeWork.dbexample.model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookDao {
    private final Connection connection;

    public BookDao(Connection connection) {
        this.connection = connection;
    }

    public Book findBookById(Integer booksId) throws SQLException {
        String sql = "select * from books where id = ?";
        PreparedStatement selectQuery = connection.prepareStatement(sql);
        selectQuery.setInt(1, booksId);
        ResultSet result = selectQuery.executeQuery();
        Book book = new Book();
        while (result.next()) {
            book.setBooksId(result.getInt("id"));
            book.setBooksTitle(result.getString("title"));
            book.setBooksAutor_book(result.getString("autor_book"));
            book.setDateAdded(result.getDate("date_added"));
        }
        return book;
    }

    public Book findBookByTitle(String title) throws SQLException {
        String sql = "select * from books where title = ?";
        PreparedStatement selectQuery = connection.prepareStatement(sql);
        selectQuery.setString(1, title);
        ResultSet result = selectQuery.executeQuery();
        Book book = new Book();
        while (result.next()) {
            book.setBooksId(result.getInt("id"));
            book.setBooksTitle(result.getString("title"));
            book.setBooksAutor_book(result.getString("autor_book"));
            book.setDateAdded(result.getDate("date_added"));
        }
        return book;
    }
}
