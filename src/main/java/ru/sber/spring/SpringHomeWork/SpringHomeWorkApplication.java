package ru.sber.spring.SpringHomeWork;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.sber.spring.SpringHomeWork.dbexample.MyDBApplicationContext;
import ru.sber.spring.SpringHomeWork.dbexample.dao.BookDao;
import ru.sber.spring.SpringHomeWork.dbexample.dao.UserDao;
import ru.sber.spring.SpringHomeWork.dbexample.model.User;

import java.time.LocalDate;

@SpringBootApplication
public class SpringHomeWorkApplication implements CommandLineRunner{
	private final BookDao bookDao;
	private final UserDao userDao;

	public SpringHomeWorkApplication(BookDao bookDao, UserDao userDao) {
		this.bookDao = bookDao;
		this.userDao = userDao;
	}

	public static void main(String[] args) {

		SpringApplication.run(SpringHomeWorkApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		userDao.addUser(new User(1, "Antonov", "ivan",
				LocalDate.of(1980, 1,25),
				"8-914-101-2480",
				"qrt12@mail.ru", "Дюна, Тревожные люди, Медвежий угол"));
		userDao.addUser(new User(2, "Fedorov", "Pavel",
				LocalDate.of(1984, 12,  1),
				"8-924-111-3434", "fdgdh@mail.ru",
				"Здесь была Бритт-Мари, Медвежий угол, Дюна"));
		userDao.getUserBooksInfo("8-914-101-2480");
	}
}
